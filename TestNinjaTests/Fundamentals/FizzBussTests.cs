﻿using NUnit.Framework;
using TestNinja.Fundamentals;

namespace TestNinjaTests.Fundamentals
{
    [TestFixture]
    public class Tests
    {
        [Test]
        [TestCase(15, "FizzBuzz")]
        [TestCase(12, "Fizz")]
        [TestCase(55, "Buzz")]
        [TestCase(56, "56")]
        [TestCase(0, "FizzBuzz")]
        public void GetOutput_WhenPassInNumber_ReturnResult(int number, string expected)
        {
            //Act
            var actual = FizzBuzz.GetOutput(number);

            //Assert
            Assert.That(actual, Is.EqualTo(expected));
        }

        [Test]
        public void GetOutput_InputIsDivisionBy3And5_ReturnFizzBuzz()
        {
            //Act
            var result = FizzBuzz.GetOutput(15);

            //Assert
            Assert.That(result, Is.EqualTo("FizzBuzz"));
        }

        [Test]
        public void GetOutput_InputIsDivisionBy3Only_ReturnFizz()
        {
            //Act
            var result = FizzBuzz.GetOutput(3);

            //Assert
            Assert.That(result, Is.EqualTo("Fizz"));
        }

        [Test]
        public void GetOutput_InputIsDivisionBy5Only_ReturnBuzz()
        {
            //Act
            var result = FizzBuzz.GetOutput(5);

            //Assert
            Assert.That(result, Is.EqualTo("Buzz"));
        }

        [Test]
        public void GetOutput_InputIsDivisionBy3Or5_ReturnTheSameNumber()
        {
            //Act
            var result = FizzBuzz.GetOutput(1);

            //Assert
            Assert.That(result, Is.EqualTo("1"));
        }
    }
}