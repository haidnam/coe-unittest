﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestNinja.Mocking.Stub;

namespace TestNinjaTests.Mocking.Stub
{
    [TestFixture]
    public class FileCheckerTests
    {
        [Test]
        public void CheckFile_CheckExtension_ReturnTrue()
        {
            //Act  
            StubExtensionManager stub = new StubExtensionManager();
            FileChecker checker = new FileChecker(stub);

            //Action  
            bool IsTrueFile = checker.CheckFile("myFile.whatever");

            //Assert  
            Assert.AreEqual(true, IsTrueFile);
        }


    }
}
