﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestNinja.Fundamentals
{
    public class SecurityHandler
    {
        private readonly ILoginService _service;
        public int UserID { get; internal set; }
        public SecurityHandler(ILoginService service)
        {
            _service = service;
        }
        public bool LoginUser(string userName, string password)
        {
            return (UserID = _service.ValidateUser(userName, password)) != 0;
        }
    }
}
