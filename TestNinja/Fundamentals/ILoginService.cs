﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestNinja.Fundamentals
{
    public interface ILoginService
    {
        int ValidateUser(string userName, string password);

    }
}
