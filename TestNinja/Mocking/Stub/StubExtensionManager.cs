﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestNinja.Mocking.Stub
{
    public class StubExtensionManager : IExtensionManager
    {
        public bool CheckExtension(string FileName)
        {
            return true;
        }
    }
}
