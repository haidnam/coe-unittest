﻿using System;

using System.Text;
using Xunit;
using TestNinja.Fundamentals;
namespace TestNinjaXunitTest.Fundamentals
{
    public class StackTests
    {
        [Fact]
        public void Push_ArgIsNull_ThrowArgumentNullException()
        {
            //Arrange
            var stack = new Stack<string>();

            //Act & Arrange
            Assert.Throws<ArgumentNullException>(() => stack.Push(null));

        }

        [Fact]
        public void Push_ValidArg_AddTheObjectToTheStack()
        {
            var stack = new Stack<string>();

            stack.Push("a");

            Assert.Equal(1, stack.Count);
        }

        [Fact]
        public void Push_EmptyStack_ReturnZero()
        {
            var stack = new Stack<string>();

            Assert.Equal(0, stack.Count);
        }

        [Fact]
        public void Pop_EmptyStack_ThrowInvalidOperationException()
        {
            var stack = new Stack<string>();
            Assert.Throws<InvalidOperationException>(() => stack.Pop());
        }

        [Fact]
        public void Pop_StackWithFewObject_ReturnObjectOnTheTop()
        {
            var stack = new Stack<string>();
            stack.Push("a");
            stack.Push("b");
            stack.Push("c");

            var result = stack.Pop();

            Assert.True(result == "c");
        }

        [Fact]
        public void Pop_StackWithFewObject_RemoveObjectOnTheTop()
        {
            var stack = new Stack<string>();
            stack.Push("a");
            stack.Push("b");
            stack.Push("c");

            var result = stack.Pop();

            Assert.True(stack.Count == 2);
        }

        [Fact]
        public void Peak_StackIsEmpty_ThrowInvalidOperationException()
        {
            var stack = new Stack<string>();
            Assert.Throws<InvalidOperationException>(() => stack.Peek());

        }

        [Fact]
        public void Peak_StackWithObjects_ReturnObjectOnTheTop()
        {
            var stack = new Stack<string>();

            stack.Push("a");
            stack.Push("b");
            stack.Push("c");

            var result = stack.Peek();

            Assert.Equal("c", result);
        }

        [Fact]
        public void Peek_StackWithObjects_DoesNotRemoveTheObjectOnTopOfTheStack()
        {
            var stack = new Stack<string>();

            stack.Push("a");
            stack.Push("b");
            stack.Push("c");

            stack.Peek();

            Assert.Equal(3, stack.Count);
        }
    }
}
