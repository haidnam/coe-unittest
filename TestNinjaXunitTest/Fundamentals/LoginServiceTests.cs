﻿using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using TestNinja.Fundamentals;
using Xunit;

namespace TestNinjaXunitTest.Fundamentals
{
    public class LoginServiceTests
    {
        //[Fact]
        //public void Should_Return_True_With_Valid_Username_And_Password()
        //{
        //    SecurityHandler handler = new SecurityHandler(new FakeLoginService());
        //    Assert.True(handler.LoginUser(string.Empty, string.Empty));
        //    Assert.Equal(5, handler.UserID);
        //}
        private Mock<ILoginService> _service;
        private SecurityHandler _handler;
        public LoginServiceTests()
        {
            _service = new Mock<ILoginService>();
            _handler = new SecurityHandler(_service.Object);
        }

        [Fact]
        public void Should_Return_True_With_Valid_Username_And_Password_Stub()
        {
            string userName = "Bob";
            string password = "Password";


            _service.Setup(vc => vc.ValidateUser(userName, password)).Returns(5);


            Assert.True(_handler.LoginUser(userName, password));
            Assert.Equal(5, _handler.UserID);

        }
    }
}
