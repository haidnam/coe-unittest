﻿using System;
using System.Collections.Generic;
using System.Text;
using Xunit;
using TestNinja.Fundamentals;

namespace TestNinjaXunitTest.Fundamentals
{

    public class FizzBussTests
    {

        [Theory]
        [InlineData(15, "FizzBuzz")]
        [InlineData(12, "Fizz")]
        [InlineData(55, "Buzz")]
        [InlineData(56, "56")]
        [InlineData(0, "FizzBuzz")]
        public void GetOutput_WhenPassInNumber_ReturnResult(int number, string expected)
        {
            //Act
            var actual = FizzBuzz.GetOutput(number);

            //Assert
            Assert.Equal(actual, expected);
        }

        [Fact]
        public void GetOutput_InputIsDivisionBy3And5_ReturnFizzBuzz()
        {
            //Act
            var result = FizzBuzz.GetOutput(15);

            //Assert
            Assert.Equal("FizzBuzz", result);
        }

        [Fact]
        public void GetOutput_InputIsDivisionBy3Only_ReturnFizz()
        {
            //Act
            var result = FizzBuzz.GetOutput(3);

            //Assert
            Assert.Equal("Fizz", result);
        }

        [Fact]
        public void GetOutput_InputIsDivisionBy5Only_ReturnBuzz()
        {
            //Act
            var result = FizzBuzz.GetOutput(5);

            //Assert
            Assert.Equal("Buzz", result);
        }

        [Fact]
        public void GetOutput_InputIsDivisionBy3Or5_ReturnTheSameNumber()
        {
            //Act
            var result = FizzBuzz.GetOutput(1);

            //Assert
            Assert.Equal("1", result);
        }
    }
}
