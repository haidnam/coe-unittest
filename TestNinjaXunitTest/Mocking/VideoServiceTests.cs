﻿using System.Collections.Generic;
using Moq;

using TestNinja.Mocking;
using Xunit;

namespace TestNinjaXunitTest.Mocking
{
    public class VideoServiceTests
    {
        private Mock<IVideoRepository> _repository;
        private VideoService _videoService;
        public VideoServiceTests()
        {
            _repository = new Mock<IVideoRepository>();
            _videoService = new VideoService(_repository.Object);
        }

        [Fact]
        public void GetUnprocessedVideosAsAsCsc_AllVideosAreProcessed_ReturnAnEmptyString()
        {
            _repository.Setup(vc => vc.GetUnprocessedVideo()).Returns(new List<Video>());

            var result = _videoService.GetUnprocessedVideosAsCsv();

            Assert.Equal("", result);
        }

        [Fact]
        public void GetUnprocessedVideosAsAsCsc_WhenCalled_ReturnAStringWithIdOfUnprocessedVideos()
        {
            _repository.Setup(vc => vc.GetUnprocessedVideo()).Returns(new List<Video>()
            {
                new Video(){Id=1, IsProcessed = false},
                new Video(){Id=3, IsProcessed = false},
            });

            var result = _videoService.GetUnprocessedVideosAsCsv();

            Assert.Equal("1,3", result);
        }

        public void Dispose()
        {
            _videoService = null;
        }
    }
}
