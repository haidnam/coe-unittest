﻿using System;
using System.Collections.Generic;
using System.Text;
using TestNinja.Mocking.Stub;
using Xunit;

namespace TestNinjaXunitTest.Mocking.Stub
{
    public class FileCheckerTests
    {
        [Fact]
        public void CheckFile_CheckExtension_ReturnTrue()
        {
            //Act  
            StubExtensionManager stub = new StubExtensionManager();
            FileChecker checker = new FileChecker(stub);

            //Action  
            var IsTrueFile = checker.CheckFile("myFile.whatever");

            //Assert  
            Assert.True(IsTrueFile);
        }
    }
}
